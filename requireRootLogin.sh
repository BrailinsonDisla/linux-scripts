
#########################################################################
#  Script     : requireRootLogin.sh
#  Author     : BrailinsonDisla
#  Date       : July 14th, 2018
#  Last Edited: July 14th, 2018 @ ~ 22:00, BrailinsonDisla
#########################################################################
# Purpose:
#	The script will edit the .bashrc file for the given users
#	and require them to login as root before they can use the
#	terminal.If login authentication fails three times, the
#	terminal will exit.
#
# Requirements:
#	Must be executed as root.
#
# Method:
#	Add usernames as arguments to the script.
#
# Syntax:
#	requireRootLogin.sh [ARGS]
#
# Notes:
#	Once the script is executed, all users whose usernames are
#	passed as arguments will be required to first login as root
#	before they can use the terminal. After three failed attempts,
#	the terminal will exit.
#########################################################################

# Check if current user is root.
if [ $UID -ne 0 ]
	then

		# Inform the user about the error.
		echo "Please make sure to login as root to run the script"

		# Exit the script with a non-zero exit status.
		exit 1
fi

# Store number of arguments.
numberOfArgs=$#

# If no user is listed as an argument for the script, the script will not execute.
if [ $numberOfArgs -eq 0 ]
	then
		# Inform the user about the error.
		echo "Please follow the script call with a list of usernames, separated by a single spaces."

		# Exit the script with a non-zero exit status.
		exit 1
fi

# A new file will be created with this path to store the root login status.
	# 0 - Root has not logged in.
	# 1 - Root has logged in.
terminalLogInStatusPath=/etc/.rootLogInStatus

# Creates the file for the root login status.
touch $terminalLogInStatusPath

# Changes the file permissions of the root login status file so that only root
# has write permission, all other users will only have read permission.
chmod 644 $terminalLogInStatusPath

# Echos 0 to root login status - Root has not logged in.
echo 0 > $terminalLogInStatusPath

# Loops through the list of users provided as arguments.
for usr in $*
	do
		# Echos code that will require root login when the terminal is opened by the user.
			# The code is written such that after 3 failed attempts to login as root, the terminal will close.
		echo "
# Reads the status of the root login.
rootLogInStatus=\$(cat $terminalLogInStatusPath)

# Starts the counter for login attempts.
declare -i attempts=0

# Checks if the status is 0 (for not logged in) or 1 (for logged in).
if [ \$rootLogInStatus -eq 0 ]
	then
		# Checks if current user is not root:
			# If it is 	- exits the Loop.
			# If it is not 	- requests root login until the user logs in as root or until three failed attempts.
		while [ \$UID -ne 0 ]
			do
				# Increases the number of attempts for first try and if further login fails.
				attempts+=1

				# Checks if the number of attempts is three (or more).
				if [ \$attempts -gt 3 ]
					then
						# Exits the termical (closes it) if attempt limit is passed.
						exit
				fi

				# Root login command.
				su
			done

		# Reset screen and return to original user.
		reset; exit
fi" >> /home/$usr/.bashrc

		# Path to script ran when user logged in (session) logs out.
		terminalLogOutPath=/home/$usr/.bash_logout

		# Echos the code that will change root log in status once user logged in logs out.
		echo ""; echo "echo 0 > $terminalLogInStatusPath" >> $terminalLogOutPath
	done

# Path to script ran when root logs in.
terminalRootLogInPath=/root/.bashrc

# Echos the code that will change root login status once root logs in.
echo "
# Echoes 1 to root login status
echo 1 > $terminalLogInStatusPath" >> $terminalRootLogInPath

#************MISSING************#
# RESTORE ROOT LOGIN STATUS /15 #
